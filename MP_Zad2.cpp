﻿#include "pch.h"
#include <iostream>
using namespace std;

class punkt
{
public:
	double _x, _y, _z;
	punkt()									: _x(0), _y(0), _z(0) {};	
	punkt(double __x, double __y, double __z)	: _x(__x), _y(__y), _z(__z) {};	
	punkt(const punkt & copy)
	{
		_x = copy._x;
		_y = copy._y;
		_z = copy._z;
	}

	punkt operator = (const punkt & przypisanie)
	{
		_x = przypisanie._x;
		_y = przypisanie._y;
		_z = przypisanie._z;
		return *this;
	}

	double& x() { return _x; };												
	double& y() { return _y; };
	double& z() { return _z; };

	const double& x() const { return _x; };									
	const double& y() const { return _y; };
	const double& z() const { return _z; };
};

class prostokat
{
public:
	double _a, _b;		
	punkt srodek;

	prostokat() : _a(0), _b(0)  {};		
	prostokat(punkt pkt, double __a, double __b)						:srodek(pkt), _a(__a), _b(__b) {};
	prostokat(double _x, double _y, double _z, double __a, double __b)	:srodek (_x, _y, _z), _a(__a), _b(__b) {};
	
	double& x() { return srodek.x(); };
	double& y() { return srodek.y(); };
	double& z() { return srodek.z(); };

	const double& x() const { return srodek.x(); };									
	const double& y() const { return srodek.y(); };
	const double& z() const { return srodek.z(); };

	double& a() { return _a; };
	const double& a() const { return _a; };

	double& b() { return _b; };
	const double& b() const { return _b; };

	double pole() { return _a * _b; };
	const double pole() const { return a() * b(); };
};


class graniastoslup
{
public:
	prostokat podstawa;
	double wysokosc;			

	graniastoslup() {};		
	graniastoslup(punkt _pkt, double _a, double _b, double _wysokosc)						:podstawa(_pkt, _a, _b), wysokosc(_wysokosc){};
	graniastoslup(double _x, double _y, double _z, double _a, double _b, double _wysokosc)	:podstawa(_x, _y, _z, _a, _b), wysokosc(_wysokosc) {};
	graniastoslup(prostokat _prostokat, double _wysokosc)								:podstawa(_prostokat), wysokosc(_wysokosc) {};

	double x() { return podstawa.x(); };
	double y() { return podstawa.y(); };
	double z() { return podstawa.z(); };
	double& a() { return podstawa.a(); };
	double& b() { return podstawa.b(); };
	double& h() { return wysokosc; };
	double objetosc() { return podstawa.pole() * wysokosc; };

	const double x() const { return podstawa.x(); };
	const double y() const { return podstawa.y(); };
	const double z() const { return podstawa.z(); };
	const double a() const { return podstawa.a(); };
	const double b() const { return podstawa.b(); };
	const double h() const { return wysokosc; };
	const double objetosc() const { return podstawa.pole() * wysokosc; };
};


int main()
{
	punkt p1, p2(1, 2, 3);
	const punkt p3(1.1, 2.2, 3.3);
	cout << "Punkt p3 \n" << endl;
	cout << p3.x() << "\t" << p3.y() << "\t" << p3.z() << endl;
	p1.x() = 1; p1.y() = 10; p1.x() = 100;
	cout << "Punkt p1\n" << endl;
	cout << p1.x() << "\t" << p1.y() << "\t" << p1.z() << endl;
	cout << "Punkt p2\n" << endl;
	cout << p2.x() << "\t" << p2.y() << "\t" << p2.z() << endl;

	prostokat pr1, pr2(1, 2, 3, 10.5, 20.5);
	const prostokat pr3(p2, 5, 5);

	cout << "Prostokat pr3\n" << endl;
	cout << pr3.x() << '\t' << pr3.y() << '\t' << pr3.z() << '\n' << pr3.a() << '\t' << pr3.b() << '\n' << pr3.pole() << endl;

	pr1.x() = 2; 	pr1.y() = 20; 	pr1.x() = 200;	pr1.a() = 10; pr1.b() = 10;
	cout << pr1.x() << "\t" << pr1.y() << "\t" << pr1.z() << "\n"
		<< pr1.a() << "\t" << pr1.b() << "\n"
		<< "Pole" << pr1.pole() << endl;
		
	graniastoslup g1, g2(1, 2, 3, 10.5, 20.5, 30.5), g3(p2, 100, 200, 300);
	const graniastoslup g4(pr3, 5);
	cout << "\nGraniastoslupy\n";
	cout << g4.x() << "\t" << g4.y() << "\t" << g4.z() << "\n"
		<< g4.a() << "\t" << g4.b() << "\t" << g4.h() << "\n"
		<<"graniastoslup 4 obj\n"<< g4.objetosc() << endl;
	g1.a() = 10; g1.b() = 10; g1.h() = 10;
	cout << g1.x() << "\t" << g1.y() << "\t" << g1.z() << "\n"
		<< g1.a() << "\t" << g1.b() << "\t" << g1.h() << "\n"
		<< "graniastoslup 1 obj\n" << g1.objetosc() << endl;

	//system("pause");
	return 0;
}
